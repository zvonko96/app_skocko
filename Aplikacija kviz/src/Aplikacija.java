 import java.awt.EventQueue;
import java.lang.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;

public class Aplikacija {
	private JFrame frmKviz;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	public int brB;
	public int brTac;
	public int brNet;
	
	ArrayList<Takmicar> ls=new ArrayList<Takmicar>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Aplikacija window = new Aplikacija();
					window.frmKviz.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}		
		});
	}
	/**
	 * Create the application.
	 */
	public Aplikacija() {
		initialize();
	}
	
	JTextArea textArea = new JTextArea();
	JRadioButton rdbtnNewRadioButton = new JRadioButton("Kareem Abdul-Jabbar");
	JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Cassius Marcellus Clay Jr");
	JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Carl Weathers");
	JRadioButton rdbtnPlivanje = new JRadioButton(" plivanje");
	JRadioButton rdbtnFudbal = new JRadioButton("  fudbal");
	JRadioButton rdbtnTenis = new JRadioButton("tenis");
	JRadioButton rdbtnCampNou = new JRadioButton("Camp Nou");
	JRadioButton rdbtnOldTrafford = new JRadioButton("Old Trafford");
	JRadioButton rdbtnWhiteHartLane = new JRadioButton(" White Hart Lane");
	JRadioButton radioButton = new JRadioButton("5");
	JRadioButton radioButton_1 = new JRadioButton("9");
	JRadioButton radioButton_2 = new JRadioButton("8");
	JRadioButton rdbtnDelPotro = new JRadioButton("Del Potro");
	JRadioButton rdbtnFederer = new JRadioButton("Federer");
	JRadioButton rdbtnDjokovic = new JRadioButton("Djokovic");
	private final JPanel panel_5 = new JPanel();
	private final JLabel lblNewLabel_1 = new JLabel("1) Koja drzava je druga po velicini u svetu?\r\n");
	private final JRadioButton rdbtnKanada = new JRadioButton("Kanada");
	private final JRadioButton rdbtnRusija = new JRadioButton("Rusija");
	private final JRadioButton rdbtnAustralija = new JRadioButton("Australija");
	private final JLabel lblNewLabel_2 = new JLabel("2) Koja je  zemlja sa najvecim brojem stanovnika  na svetu?");
	private final JRadioButton rdbtnKina = new JRadioButton("Kina");
	private final JRadioButton rdbtnRusija_1 = new JRadioButton("Rusija");
	private final JRadioButton rdbtnSad = new JRadioButton("SAD");
	private final JLabel lblNewLabel_3 = new JLabel("3) Koja je najveca reka na svetu?");
	private final JRadioButton rdbtnAmazon = new JRadioButton("Amazon");
	private final JRadioButton rdbtnMisisipi = new JRadioButton("Misisipi");
	private final JRadioButton rdbtnJangcekjang = new JRadioButton("Jangcekjang");
	private final JLabel lblNewLabel_4 = new JLabel("4) U blizini kog grada se nalazi Djavolja varos?");
	private final JRadioButton rdbtnKursumlija = new JRadioButton("Kursumlija\r\n");
	private final JRadioButton rdbtnLeskovac = new JRadioButton("Leskovac");
	private final JRadioButton rdbtnProkuplje = new JRadioButton("Prokuplje");
	private final JLabel lblNajsevernijiGradU = new JLabel("5) Najseverniji grad u Srbiji je?");
	private final JRadioButton rdbtnSubotica = new JRadioButton("Subotica");
	private final JRadioButton rdbtnVrsac = new JRadioButton("Vrsac");
	private final JRadioButton rdbtnSombor = new JRadioButton("Sombor");
	private final JButton btnOdgovori = new JButton("Osvojena suma");
	private JTextField Field_1;
	private JTextField Field_3;
	private JTextField Field_4;
	private JTextField Field_2;
	private JTextField Field_5;
	private JTextField Field_7;
	private JTextField Field_6;
	private JTextField Field_8;
	private JTextField Field_10;
	private JTextField Field_12;
	private JTextField Field_11;
	private JTextField Field_9;
	private JTextField Field_13;
	private JTextField Field_15;
	private JTextField Field_14;
	private JTextField Field_16;
	private JTextField Field_17;
	private JTextField Field_19;
	private JTextField Field_20;
	private JTextField Field_18;
	private JTextField Field_22;
	private JTextField Field_24;
	private JTextField Field_23;
	private JTextField Field_21;
	private final JButton button_6 = new JButton("Proveri");
	private final JButton button_7 = new JButton("Proveri");
	private final JButton button_8 = new JButton("Proveri");
	private final JButton button_9 = new JButton("Proveri");
	private final JButton button_10 = new JButton("Proveri");
	private final JButton button_11 = new JButton("Proveri");
	private final JLabel lblOsvojenaSuma = new JLabel("Osvojena suma");
	private final JTextField Field_29 = new JTextField();
	private final JPanel panel_8 = new JPanel();
	private final JTextField Field_30 = new JTextField();
	private final JTextField Field_31 = new JTextField();
	private final JTextField Field_32 = new JTextField();
	private final JTextField Field_33 = new JTextField();
	private final JLabel label_7 = new JLabel("Resenje:");
	private final JTextField Field_25 = new JTextField();
	private final JTextField Field_26 = new JTextField();
	private final JTextField Field_27 = new JTextField();
	private final JTextField Field_28 = new JTextField();
	private final JButton btnKombinacija = new JButton("Kombinacija");
	JRadioButton Button_2 = new JRadioButton("");
	JRadioButton Button_3 = new JRadioButton("");
	JRadioButton Button_4 = new JRadioButton("");
	JRadioButton Button_1 = new JRadioButton("");
	JRadioButton Button_5 = new JRadioButton("");
	JRadioButton Button_6 = new JRadioButton("");
	JRadioButton Button_7 = new JRadioButton("");
	JRadioButton Button_8 = new JRadioButton("");
	JRadioButton Button_21 = new JRadioButton("");
	JRadioButton Button_22 = new JRadioButton("");
	JRadioButton Button_23 = new JRadioButton("");
	JRadioButton Button_24 = new JRadioButton("");
	JRadioButton Button_20 = new JRadioButton("");
	JRadioButton Button_19 = new JRadioButton("");
	JRadioButton Button_18 = new JRadioButton("");
	JRadioButton Button_17 = new JRadioButton("");
	JRadioButton Button_16 = new JRadioButton("");
	JRadioButton Button_15 = new JRadioButton("");
	JRadioButton Button_14 = new JRadioButton("");
	JRadioButton Button_13 = new JRadioButton("");
	JRadioButton Button_9 = new JRadioButton("");
	JRadioButton Button_10 = new JRadioButton("");
	JRadioButton Button_11 = new JRadioButton("");
	JRadioButton Button_12 = new JRadioButton("");
	private final JScrollPane scrollPane = new JScrollPane();

	private void initialize() {
		frmKviz = new JFrame();
		frmKviz.setFont(new Font("Arial", Font.BOLD, 14));
		frmKviz.setTitle("Kviz");
		frmKviz.setBackground(new Color(51, 0, 0));
		frmKviz.setBounds(100, 100, 590, 412);
		frmKviz.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKviz.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 581, 380);
		frmKviz.getContentPane().add(panel);
		panel.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		tabbedPane.setForeground(new Color(0, 0, 0));
		tabbedPane.setBackground(new Color(255, 255, 0));
		tabbedPane.setBounds(0, 0, 581, 380);
		panel.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBackground(new Color(218, 165, 32));
		tabbedPane.addTab("Unos podataka", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblIme = new JLabel("Ime:");
		lblIme.setBounds(10, 32, 46, 23);
		lblIme.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 13));
		panel_1.add(lblIme);
		
		JLabel lblPrezime = new JLabel("Prezime:");
		lblPrezime.setBounds(10, 67, 62, 25);
		lblPrezime.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 13));
		panel_1.add(lblPrezime);
		
		JLabel lblGodine = new JLabel("Godine:");
		lblGodine.setBounds(10, 103, 62, 20);
		lblGodine.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 13));
		panel_1.add(lblGodine);
		
		JButton btnNewButton = new JButton("Unesi podatke");
		btnNewButton.setBounds(10, 137, 167, 34);
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String i=textField.getText();
				String p=textField_1.getText();
				String g=textField_2.getText();
				try {
					int god = Integer.parseInt(g);
					Takmicar t = new Takmicar(i, p, god);
					ls.add(t);
				} catch (Exception ex)
				{
					JOptionPane.showMessageDialog(null,"Pogresan format!!!");
				}			
				}		
		});
		
			btnNewButton.setBackground(new Color(143, 188, 143));
			btnNewButton.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 13));
			panel_1.add(btnNewButton);
			
			textField = new JTextField();
			textField.setBounds(66, 33, 111, 23);
			panel_1.add(textField);
			textField.setColumns(10);
			
			textField_1 = new JTextField();
			textField_1.setBounds(66, 69, 111, 23);
			textField_1.setColumns(10);
			panel_1.add(textField_1);
			
			textField_2 = new JTextField();
			textField_2.setBounds(66, 103, 111, 23);
			textField_2.setColumns(10);
			panel_1.add(textField_2);
			
			JLabel lblUpustvo = new JLabel("Uputstvo");
			lblUpustvo.setBounds(326, 3, 71, 34);
			lblUpustvo.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 13));
			panel_1.add(lblUpustvo);
			scrollPane.setBounds(180, 32, 386, 309);
			
			panel_1.add(scrollPane);
			
			JTextArea txtrUnesiteVaseIme = new JTextArea();
			scrollPane.setViewportView(txtrUnesiteVaseIme);
			txtrUnesiteVaseIme.setFont(new Font("Arial", Font.PLAIN, 13));
			txtrUnesiteVaseIme.setBackground(new Color(255, 255, 204));
			txtrUnesiteVaseIme.setText("\u0410\u043A\u043E \u0436\u0435\u043B\u0438\u0442\u0435 \u0434\u0430 \u0443\u0447\u0435\u0441\u0442\u0432\u0443\u0458\u0435\u0442\u0435 \u0443 \u043D\u0430\u0448 \u043A\u0432\u0438\u0437 \u043D\u043E\u043F\u0445\u043E\u0434\u043D\u043E \u0458\u0435 \u043F\u0440\u0432\u043E\r\n\u043F\u043E\u043F\u0443\u043D\u0438\u0442\u0438 \u043F\u0440\u0430\u0437\u043D\u0430 \u043F\u043E\u0459\u0430 \u0442\u0458. \u043F\u043E\u0442\u0440\u0435\u0431\u043D\u043E \u0458\u0435 \u0434\u0430 \u0443\u043D\u0435\u0441\u0435\u0442\u0435 \u0432\u0430\u0448\u0435 \r\n\u043F\u043E\u0434\u0430\u0442\u043A\u0435 \u0438 \u0442\u043E: \u0438\u043C\u0435, \u043F\u0440\u0435\u0437\u0438\u043C\u0435 \u0438 \u043A\u043E\u043B\u0438\u043A\u043E \u0433\u043E\u0434\u0438\u043D\u0430 \u0438\u043C\u0430\u0442\u0435. \r\n\u041A\u0430\u0434\u0430 \u0441\u0432\u0435 \u0442\u043E \u043F\u043E\u043F\u0443\u043D\u0438\u0442\u0435 \u043C\u043E\u0440\u0430\u0442\u0435 \u043A\u043B\u0438\u043A\u043D\u0443\u0442\u0438 \u043D\u0430 \u0434\u0443\u0433\u043C\u0435 \r\n\"Unesi podatke\" \u043A\u0430\u043A\u043E \u0431\u0438 \u0441\u0435 \u0432\u0430\u0448\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u0443\u0447\u0438\u0442\u0430\u043B\u0438. \u041D\u0430 \r\n\u0434\u0440\u0443\u0433\u043E\u043C \u0442\u0430\u0431\u0443 \u043D\u0430\u043B\u0430\u0437\u0435 \u0441\u0435 \u043F\u0438\u0442\u0430\u045A\u0430 \u0441\u0430 \u043F\u043E\u043D\u0443\u0452\u0435\u043D\u0438\u043C \u043E\u0434\u0433\u043E\u0432\u043E\u0440\u0438\u043C\u0430\r\n\u0438\u0437 \u043E\u0431\u043B\u0430\u0441\u0442\u0438  \u0441\u043F\u043E\u0440\u0442\u0430, \u043D\u0430 \u043F\u0438\u0442\u0430\u045A\u0438\u043C\u0430 \u0441\u0435 \u043E\u0434\u0433\u043E\u0432\u0430\u0440\u0430 \u0442\u0430\u043A\u043E \u0448\u0442\u043E \u0441\u0435 \r\n\u043E\u0434\u0430\u0431\u0435\u0440\u0435 \u0458\u0435\u0434\u0430\u043D \u043E\u0434 \u0442\u0440\u0438 \u043F\u043E\u043D\u0443\u0452\u0435\u043D\u0430 \u043E\u0434\u0433\u043E\u0432\u043E\u0440\u0430. \u0421\u0432\u0430\u043A\u0438 \u0442\u0430\u0447\u0430\u043D \r\n\u043E\u0434\u0433\u043E\u0432\u043E\u0440 \u0434\u043E\u043D\u043E\u0441\u0438 \u0442\u0430\u043A\u043C\u0438\u0447\u0430\u0440\u0443 \u043F\u043E 500 \u0434\u0438\u043D\u0430\u0440\u0430 \u0434\u043E\u043A \u0441\u0432\u0430\u043A\u0438 \r\n\u043F\u043E\u0433\u0440\u0435\u0448\u0430\u043D \u043E\u0434\u0433\u043E\u0432\u043E\u0440 \u043E\u0434\u0443\u0437\u0438\u043C\u0430 \u043F\u043E 100  \u0434\u0438\u043D\u0430\u0440\u0430.\r\n\r\n\u041D\u0430 \u0442\u0440\u0435\u045B\u0435\u043C \u0442\u0430\u0431\u0443  \u043D\u0430\u043B\u0430\u0437\u0435 \u0441\u0435 \u043F\u0438\u0442\u0430\u045A\u0430 \u0438\u0437 \u043E\u0431\u043B\u0430\u0441\u0442\u0438 \u0433\u0435\u043E\u0433\u0440\u0430\u0444\u0438\u0458\u0435, \u043D\u0430\r\n\u043A\u043E\u0458\u0438\u043C\u0430 \u0442\u0430\u043A\u043E\u0452\u0435 \u043E\u0434\u0433\u043E\u0432\u0430\u0440\u0430\u0442\u0435 \u043A\u0430\u043E \u0438 \u043D\u0430 \u043F\u0440\u0435\u0442\u0445\u043E\u0434\u043D\u0430 \u043F\u0438\u0442\u0430\u045A\u0430.\r\n\r\n\u041D\u0430 \u0447\u0435\u0442\u0432\u0440\u0442\u043E\u043C \u0442\u0430\u0431\u0443 \u043D\u0430\u043B\u0430\u0437\u0438 \u0441\u0435 \u0438\u0433\u0440\u0430 \u043A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0430. \u041A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0430\r\n\u0458\u0435 \u0438\u0433\u0440\u0430 \u0441\u0430 \u0431\u043E\u0458\u0430\u043C\u0430.  \u0423 \u043E\u0432\u043E\u0458 \u0438\u0433\u0440\u0438 \u043F\u043E\u0442\u0440\u0435\u0431\u043D\u043E \u0458\u0435 \u0434\u0430 \u0442\u0430\u043A\u043C\u0438\u0447\u0430\u0440 \u043F\u0440\u0432\u043E \r\n\u043A\u043B\u0438\u043A\u043D\u0435 \u043D\u0430 \u0434\u0443\u0433\u043C\u0435 \u201CKombinacija\u201D \u043A\u0430\u043A\u043E \u0431\u0438 \u0434\u043E\u0431\u0438\u043E \u043D\u0435\u043A\u0443 \r\n\u043A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0443 \u043E\u0434 \u0447\u0435\u0442\u0438\u0440\u0438 \u0431\u043E\u0458\u0430 \u043A\u043E\u0458\u0443 \u0442\u0440\u0435\u0431\u0430 \u043F\u043E\u0433\u043E\u0434\u0438\u0442\u0438. \u041A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0435 \r\n\u0441\u0435 \u0431\u0438\u0440\u0430\u0458\u0443 \u0438\u0437 \u0441\u043A\u0443\u043F\u0430 \u043E\u0434 \u0448\u0435\u0441\u0442 \u0431\u043E\u0458\u0430, \u0430 \u043E\u0434\u0440\u0435\u045B\u0435\u043D\u0430 \u0431\u043E\u0458\u0430 \u0441\u0435 \u043C\u043E\u0436\u0435 \r\n\u043F\u043E\u0458\u0430\u0432\u0459\u0438\u0432\u0430\u0442\u0438 \u0438 \u0432\u0438\u0448\u0435 \u043F\u0443\u0442\u0430. \u0418\u043C\u0430\u0442\u0435 \u0448\u0435\u0441\u0442 \u043F\u043E\u043A\u0443\u0448\u0430\u0458\u0430 \u0434\u0430 \u043F\u043E\u0433\u043E\u0434\u0438\u0442\u0435 \r\n\u043A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0443,  \u043D\u0430\u043A\u043E\u043D \u0441\u0432\u0430\u043A\u0435 \u043A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0435 \u043C\u043E\u0440\u0430\u0442\u0435 \u043A\u043B\u0438\u043A\u043D\u0443\u0442\u0438 \u043D\u0430 \r\n\u0434\u0443\u0433\u043C\u0435 \u201CProveri \u201C \u043A\u0430\u043A\u043E \u0431\u0438  \u0440\u0430\u0447\u0443\u043D\u0430\u0440 \u043F\u0440\u0438\u043A\u0430\u0437\u0430\u043E \u043A\u043E\u043B\u0438\u043A\u043E \u0431\u043E\u0458\u0430 \u0441\u0442\u0435 \r\n\u043F\u043E\u0433\u043E\u0434\u0438\u043B\u0438 \u0438 \u043A\u043E\u043B\u0438\u043A\u043E \u043E\u0434 \u0442\u0438\u0445 \u0431\u043E\u0458\u0430 \u0458\u0435 \u043D\u0430 \u043F\u0440\u0430\u0432\u043E\u0458 \u043F\u043E\u0437\u0438\u0446\u0438\u0458\u0438 (\u0430\u043A\u043E \u0458\u0435 \r\n\u0431\u043E\u0458\u0430 \u043D\u0430 \u043F\u0440\u0430\u0432\u043E\u0458 \u043F\u043E\u0437\u0438\u0446\u0438\u0458\u0438 \u043F\u043E\u0458\u0430\u0432\u0438\u045B\u0435 \u0441\u0435 \u0446\u0440\u0432\u0435\u043D\u0430 \u0431\u043E\u0458\u0430, \u0430\u043A\u043E \u0458\u0435 \u0431\u043E\u0458\u0430 \r\n\u0442\u0443 \u0430 \u043D\u0438\u0458\u0435 \u043D\u0430 \u043F\u0440\u0430\u0432\u043E\u0458 \u043F\u043E\u0437\u0438\u0446\u0438\u0458\u0438 \u043E\u043D\u0434\u0430 \u045B\u0435 \u0441\u0435 \u043F\u043E\u0458\u0430\u0432\u0438\u0442\u0438 \u0446\u0440\u043D\u0430 \u0431\u043E\u0458\u0430). \r\n\u041D\u0430\u043A\u043E\u043D \u0442\u043E\u0433\u0430 \u0442\u0430\u043A\u043C\u0438\u0447\u0430\u0440 \u0442\u0440\u0435\u0431\u0430 \u043E\u0434\u0430\u0431\u0440\u0430\u0442\u0438 4 \u0431\u043E\u0458\u0435 \u043F\u043E \u0441\u0432\u043E\u0458\u043E\u0458 \u0436\u0435\u0459\u0438 \r\n\u0438 \u043A\u043B\u0438\u043A\u043D\u0443\u0442\u0438 \u043D\u0430 \u0434\u0443\u0433\u043C\u0435 \u201CProveri \u201C. \u0417\u0430 \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u043F\u0440\u043E\u043D\u0430\u043B\u0430\u0436\u0435\u045A\u0435 \r\n\u043A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0435 \u0438\u0437 \u043F\u0440\u0432\u043E\u0433 \u043F\u043E\u043A\u0443\u0448\u0430\u0458\u0430 \u0434\u043E\u0431\u0438\u0458\u0430\u0442\u0435 1000 \u0434\u0438\u043D, \u0438\u0437 \u0434\u0440\u0443\u0433\u043E\u0433 \r\n\u043F\u043E\u043A\u0443\u0448\u0430\u0458\u0430 800 \u0434\u0438\u043D, \u0438\u0437 \u0442\u0440\u0435\u045B\u0435\u0433 600\u0434\u0438\u043D , \u0447\u0435\u0442\u0432\u0440\u0442\u043E\u0433 400\u0434\u0438\u043D, \u043F\u0435\u0442\u043E\u0433 \r\n300\u0434\u0438\u043D \u0438 \u0430\u043A\u043E \u043F\u043E\u0433\u043E\u0434\u0438\u0442\u0435 \u0438\u0437 \u043F\u043E\u0441\u043B\u0435\u0434\u045A\u0435\u0433 \u043F\u043E\u043A\u0443\u0448\u0430\u0458\u0430 \u0434\u043E\u0431\u0438\u0458\u0430\u0442\u0435 \r\n100 \u0434\u0438\u043D\u0430\u0440\u0430. \u0410\u043A\u043E \u043D\u0435 \u043F\u043E\u0433\u043E\u0434\u0438\u0442\u0435 \u043A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0443 \u0438\u0437 \u043F\u043E\u0441\u043B\u0435\u0434\u045A\u0435\u0433 \r\n\u043F\u043E\u043A\u0443\u0448\u0430\u0458\u0430 \u0434\u043E\u0431\u0438\u045B\u0435\u0442\u0435 \u043F\u0440\u0430\u0432\u0443\u043A\u043E\u043C\u0431\u0438\u043D\u0430\u0446\u0438\u0458\u0443 \u0443 \u043F\u043E\u0459\u0435 \u0440\u0435\u0448\u0435\u045A\u0435.\r\n\r\n\u041D\u0430 \u043F\u043E\u0441\u043B\u0435\u0434\u045A\u0435\u043C \u0442\u0430\u0431\u0443 \u043C\u043E\u0436\u0435\u0442\u0435 \u0432\u0438\u0434\u0435\u0442\u0438 \u0441\u0443\u043C\u0443 \u043A\u043E\u0458\u0443 \u0441\u0442\u0435 \u043E\u0441\u0432\u043E\u0458\u0438\u043B\u0438\r\n\u043A\u043B\u0438\u043A\u043E\u043C \u043D\u0430 \u0434\u0443\u0433\u043C\u0435 \u201COsvojena suma\u201D.\r\n");
		ButtonGroup grupa = new ButtonGroup();
		ButtonGroup grupa1 = new ButtonGroup();
		ButtonGroup grupa2 = new ButtonGroup();	
		ButtonGroup grupa3 = new ButtonGroup();	
		ButtonGroup grupa4 = new ButtonGroup();	
			
			JPanel panel_2 = new JPanel();
			panel_2.setForeground(new Color(153, 0, 0));
			panel_2.setBackground(new Color(218, 165, 32));
			panel_2.setToolTipText("");
			tabbedPane.addTab("Sport", null, panel_2, null);
			panel_2.setLayout(null);
			
			JLabel label_1 = new JLabel("1) Pre nego sto je promenio ime, Muhammad Ali se zvao:");
			label_1.setForeground(new Color(0, 0, 0));
			label_1.setFont(new Font("Arial", Font.BOLD, 13));
			label_1.setBounds(10, 2, 395, 23);
			panel_2.add(label_1);
			rdbtnNewRadioButton.setForeground(new Color(153, 0, 0));
			rdbtnNewRadioButton.setBackground(new Color(218, 165, 32));		
			
			rdbtnNewRadioButton.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnNewRadioButton.setBounds(10, 32, 171, 23);
			panel_2.add(rdbtnNewRadioButton);
			rdbtnNewRadioButton_1.setForeground(new Color(153, 0, 0));
			rdbtnNewRadioButton_1.setBackground(new Color(218, 165, 32));
			
			rdbtnNewRadioButton_1.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnNewRadioButton_1.setBounds(193, 32, 192, 23);
			panel_2.add(rdbtnNewRadioButton_1);
			rdbtnNewRadioButton_2.setForeground(new Color(153, 0, 0));
			rdbtnNewRadioButton_2.setBackground(new Color(218, 165, 32));
			
			rdbtnNewRadioButton_2.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnNewRadioButton_2.setBounds(404, 32, 123, 23);
			panel_2.add(rdbtnNewRadioButton_2);
			
			JLabel label_2 = new JLabel("2) Kojim sportom se bavi Novak Djokovic?");
			label_2.setForeground(new Color(0, 0, 0));
			label_2.setFont(new Font("Arial", Font.BOLD, 13));
			label_2.setBounds(10, 62, 350, 25);
			panel_2.add(label_2);
			rdbtnPlivanje.setForeground(new Color(153, 0, 0));
			rdbtnPlivanje.setBackground(new Color(218, 165, 32));
			
			rdbtnPlivanje.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnPlivanje.setBounds(10, 94, 101, 23);
			panel_2.add(rdbtnPlivanje);
			rdbtnFudbal.setForeground(new Color(153, 0, 0));
			rdbtnFudbal.setBackground(new Color(218, 165, 32));
			
			rdbtnFudbal.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnFudbal.setBounds(193, 94, 84, 23);
			panel_2.add(rdbtnFudbal);
			rdbtnTenis.setForeground(new Color(153, 0, 0));
			rdbtnTenis.setBackground(new Color(218, 165, 32));
			
			rdbtnTenis.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnTenis.setBounds(404, 99, 59, 23);
			panel_2.add(rdbtnTenis);
			
			JLabel label_3 = new JLabel("3) Na kom stadionu igra FK Manchester United?");
			label_3.setForeground(new Color(0, 0, 0));
			label_3.setFont(new Font("Arial", Font.BOLD, 13));
			label_3.setBounds(10, 129, 423, 30);
			panel_2.add(label_3);
			rdbtnCampNou.setForeground(new Color(153, 0, 0));
			rdbtnCampNou.setBackground(new Color(218, 165, 32));
			
			rdbtnCampNou.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnCampNou.setBounds(10, 166, 109, 23);
			panel_2.add(rdbtnCampNou);
			rdbtnOldTrafford.setForeground(new Color(153, 0, 0));
			rdbtnOldTrafford.setBackground(new Color(218, 165, 32));
			
			rdbtnOldTrafford.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnOldTrafford.setBounds(193, 166, 109, 23);
			panel_2.add(rdbtnOldTrafford);
			rdbtnWhiteHartLane.setForeground(new Color(153, 0, 0));
			rdbtnWhiteHartLane.setBackground(new Color(218, 165, 32));
			
			rdbtnWhiteHartLane.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnWhiteHartLane.setBounds(404, 166, 171, 23);
			panel_2.add(rdbtnWhiteHartLane);
			
			JLabel label_4 = new JLabel("4) Koliko olimpijskih zlatnih medalja je osvojio Usain Bolt?");
			label_4.setForeground(new Color(0, 0, 0));
			label_4.setFont(new Font("Arial", Font.BOLD, 13));
			label_4.setBounds(10, 201, 395, 23);
			panel_2.add(label_4);
			radioButton.setForeground(new Color(153, 0, 0));
			radioButton.setBackground(new Color(218, 165, 32));
			
			radioButton.setFont(new Font("Arial", Font.BOLD, 13));
			radioButton.setBounds(10, 231, 41, 23);
			panel_2.add(radioButton);
			radioButton_1.setForeground(new Color(153, 0, 0));
			radioButton_1.setBackground(new Color(218, 165, 32));
			
			radioButton_1.setFont(new Font("Arial", Font.BOLD, 13));
			radioButton_1.setBounds(193, 231, 41, 23);
			panel_2.add(radioButton_1);
			radioButton_2.setForeground(new Color(153, 0, 0));
			radioButton_2.setBackground(new Color(218, 165, 32));
			
			radioButton_2.setFont(new Font("Arial", Font.BOLD, 13));
			radioButton_2.setBounds(404, 231, 50, 23);
			panel_2.add(radioButton_2);
			
			JLabel label_5 = new JLabel("5) Koji teniser je osvojio US Open 2009. godine?");
			label_5.setBackground(new Color(51, 255, 153));
			label_5.setForeground(new Color(0, 0, 0));
			label_5.setFont(new Font("Arial", Font.BOLD, 13));
			label_5.setBounds(10, 274, 350, 33);
			panel_2.add(label_5);
			rdbtnDelPotro.setForeground(new Color(153, 0, 0));
			rdbtnDelPotro.setBackground(new Color(218, 165, 32));
			
			rdbtnDelPotro.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnDelPotro.setBounds(10, 308, 123, 23);
			panel_2.add(rdbtnDelPotro);
			rdbtnFederer.setForeground(new Color(153, 0, 0));
			rdbtnFederer.setBackground(new Color(218, 165, 32));
			
			rdbtnFederer.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnFederer.setBounds(193, 308, 105, 23);
			panel_2.add(rdbtnFederer);
			rdbtnDjokovic.setForeground(new Color(153, 0, 0));
			rdbtnDjokovic.setBackground(new Color(218, 165, 32));			
			
			rdbtnDjokovic.setFont(new Font("Arial", Font.BOLD, 13));
			rdbtnDjokovic.setBounds(404, 308, 79, 23);
			panel_2.add(rdbtnDjokovic);
			grupa.add(rdbtnNewRadioButton_2);
			grupa.add(rdbtnNewRadioButton);
			grupa.add(rdbtnNewRadioButton_1);
			grupa1.add(rdbtnTenis);
			grupa1.add(rdbtnFudbal);
			grupa1.add(rdbtnPlivanje);
			grupa2.add(rdbtnOldTrafford);
			grupa2.add(rdbtnWhiteHartLane);
			grupa2.add(rdbtnCampNou);
			grupa3.add(radioButton);
			grupa3.add(radioButton_1);
			grupa3.add(radioButton_2);
			grupa4.add(rdbtnDelPotro);
			grupa4.add(rdbtnDjokovic);
			grupa4.add(rdbtnFederer);
		ButtonGroup grupa5 = new ButtonGroup();
		ButtonGroup grupa6 = new ButtonGroup();
		ButtonGroup grupa7 = new ButtonGroup();
		ButtonGroup grupa8 = new ButtonGroup();
		ButtonGroup grupa9 = new ButtonGroup();
		panel_5.setBackground(new Color(218, 165, 32));
		
		tabbedPane.addTab("Geografija", null, panel_5, null);
		panel_5.setLayout(null);
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 13));
		lblNewLabel_1.setBounds(10, 11, 364, 20);
		
		panel_5.add(lblNewLabel_1);
		rdbtnKanada.setForeground(new Color(153, 0, 0));
		rdbtnKanada.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnKanada.setBackground(new Color(218, 165, 32));
		rdbtnKanada.setBounds(20, 38, 109, 23);
		
		panel_5.add(rdbtnKanada);
		rdbtnRusija.setForeground(new Color(153, 0, 0));
		rdbtnRusija.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnRusija.setBackground(new Color(218, 165, 32));
		rdbtnRusija.setBounds(175, 38, 109, 23);
		
		panel_5.add(rdbtnRusija);
		rdbtnAustralija.setForeground(new Color(153, 0, 0));
		rdbtnAustralija.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnAustralija.setBackground(new Color(218, 165, 32));
		rdbtnAustralija.setBounds(366, 38, 109, 23);
		
		panel_5.add(rdbtnAustralija);
		lblNewLabel_2.setForeground(new Color(0, 0, 0));
		lblNewLabel_2.setFont(new Font("Arial", Font.BOLD, 13));
		lblNewLabel_2.setBounds(10, 80, 454, 18);
		
		panel_5.add(lblNewLabel_2);
		rdbtnKina.setForeground(new Color(153, 0, 0));
		rdbtnKina.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnKina.setBackground(new Color(218, 165, 32));
		rdbtnKina.setBounds(175, 105, 109, 23);
		
		panel_5.add(rdbtnKina);
		rdbtnRusija_1.setForeground(new Color(153, 0, 0));
		rdbtnRusija_1.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnRusija_1.setBackground(new Color(218, 165, 32));
		rdbtnRusija_1.setBounds(20, 105, 109, 23);
		
		panel_5.add(rdbtnRusija_1);
		rdbtnSad.setForeground(new Color(153, 0, 0));
		rdbtnSad.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnSad.setBackground(new Color(218, 165, 32));
		rdbtnSad.setBounds(366, 105, 109, 23);
		
		panel_5.add(rdbtnSad);
		lblNewLabel_3.setForeground(new Color(0, 0, 0));
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 13));
		lblNewLabel_3.setBounds(10, 152, 364, 21);
		
		panel_5.add(lblNewLabel_3);
		rdbtnAmazon.setForeground(new Color(153, 0, 0));
		rdbtnAmazon.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnAmazon.setBackground(new Color(218, 165, 32));
		rdbtnAmazon.setBounds(20, 180, 109, 23);
		
		panel_5.add(rdbtnAmazon);
		rdbtnMisisipi.setForeground(new Color(153, 0, 0));
		rdbtnMisisipi.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnMisisipi.setBackground(new Color(218, 165, 32));
		rdbtnMisisipi.setBounds(175, 180, 109, 23);
		
		panel_5.add(rdbtnMisisipi);
		rdbtnJangcekjang.setForeground(new Color(153, 0, 0));
		rdbtnJangcekjang.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnJangcekjang.setBackground(new Color(218, 165, 32));
		rdbtnJangcekjang.setBounds(366, 180, 109, 23);
		
		panel_5.add(rdbtnJangcekjang);
		lblNewLabel_4.setForeground(new Color(0, 0, 0));
		lblNewLabel_4.setFont(new Font("Arial", Font.BOLD, 13));
		lblNewLabel_4.setBounds(10, 222, 420, 23);
		
		panel_5.add(lblNewLabel_4);
		rdbtnKursumlija.setForeground(new Color(153, 0, 0));
		rdbtnKursumlija.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnKursumlija.setBackground(new Color(218, 165, 32));
		rdbtnKursumlija.setBounds(366, 245, 109, 23);
		
		panel_5.add(rdbtnKursumlija);
		rdbtnLeskovac.setForeground(new Color(153, 0, 0));
		rdbtnLeskovac.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnLeskovac.setBackground(new Color(218, 165, 32));
		rdbtnLeskovac.setBounds(20, 245, 109, 23);
		
		panel_5.add(rdbtnLeskovac);
		rdbtnProkuplje.setForeground(new Color(153, 0, 0));
		rdbtnProkuplje.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnProkuplje.setBackground(new Color(218, 165, 32));
		rdbtnProkuplje.setBounds(175, 245, 109, 23);
		
		panel_5.add(rdbtnProkuplje);
		lblNajsevernijiGradU.setForeground(new Color(0, 0, 0));
		lblNajsevernijiGradU.setFont(new Font("Arial", Font.BOLD, 13));
		lblNajsevernijiGradU.setBounds(10, 275, 499, 26);
		
		panel_5.add(lblNajsevernijiGradU);
		rdbtnSubotica.setForeground(new Color(153, 0, 0));
		rdbtnSubotica.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnSubotica.setBackground(new Color(218, 165, 32));
		rdbtnSubotica.setBounds(175, 308, 109, 23);
		
		panel_5.add(rdbtnSubotica);
		rdbtnVrsac.setForeground(new Color(153, 0, 0));
		rdbtnVrsac.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnVrsac.setBackground(new Color(218, 165, 32));
		rdbtnVrsac.setBounds(366, 308, 109, 23);
		
		panel_5.add(rdbtnVrsac);
		rdbtnSombor.setForeground(new Color(153, 0, 0));
		rdbtnSombor.setFont(new Font("Arial", Font.BOLD, 13));
		rdbtnSombor.setBackground(new Color(218, 165, 32));
		rdbtnSombor.setBounds(20, 308, 109, 23);
		
		ButtonGroup grupa10 = new ButtonGroup();
		ButtonGroup grupa11 = new ButtonGroup();
		ButtonGroup grupa12 = new ButtonGroup();
		ButtonGroup grupa13 = new ButtonGroup();
		ButtonGroup grupa14 = new ButtonGroup();
		
		grupa10.add(rdbtnKanada);
		grupa10.add(rdbtnRusija);
		grupa10.add(rdbtnAustralija);		
		
		grupa11.add(rdbtnRusija_1);
		grupa11.add(rdbtnSad);
		grupa11.add(rdbtnKina);		
		
		grupa12.add(rdbtnAmazon);
		grupa12.add(rdbtnMisisipi);
		grupa12.add(rdbtnJangcekjang);	
		
		grupa13.add(rdbtnLeskovac);
		grupa13.add(rdbtnProkuplje);
		grupa13.add(rdbtnKursumlija);	
		
		grupa14.add(rdbtnSombor);
		grupa14.add(rdbtnSubotica);
		grupa14.add(rdbtnVrsac);
			
		panel_5.add(rdbtnSombor);
		
		ButtonGroup grupa15 = new ButtonGroup();
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(new Color(218, 165, 32));
		tabbedPane.addTab("Kombinacija ", null, panel_6, null);
		panel_6.setLayout(null);
		
		JPanel panel_7 = new JPanel();
		panel_7.setLayout(null);
		panel_7.setBackground(new Color(218, 165, 32));
		panel_7.setBounds(10, 44, 257, 305);
		panel_6.add(panel_7);
		
		Field_1 = new JTextField();
		Field_1.setForeground(Color.WHITE);
		Field_1.setEnabled(false);
		Field_1.setEditable(false);
		Field_1.setColumns(10);
		Field_1.setBackground(Color.WHITE);
		Field_1.setBounds(207, 12, 14, 11);
		panel_7.add(Field_1);
		
		Field_3 = new JTextField();
		Field_3.setForeground(Color.WHITE);
		Field_3.setEnabled(false);
		Field_3.setEditable(false);
		Field_3.setColumns(10);
		Field_3.setBackground(Color.WHITE);
		Field_3.setBounds(207, 33, 14, 11);
		panel_7.add(Field_3);
		
		Field_4 = new JTextField();
		Field_4.setForeground(Color.WHITE);
		Field_4.setEnabled(false);
		Field_4.setEditable(false);
		Field_4.setColumns(10);
		Field_4.setBackground(Color.WHITE);
		Field_4.setBounds(231, 33, 14, 11);
		panel_7.add(Field_4);
		
		Field_2 = new JTextField();
		Field_2.setForeground(Color.WHITE);
		Field_2.setEnabled(false);
		Field_2.setEditable(false);
		Field_2.setColumns(10);
		Field_2.setBackground(Color.WHITE);
		Field_2.setBounds(231, 12, 14, 11);
		panel_7.add(Field_2);
				
		Button_2.setHorizontalAlignment(SwingConstants.CENTER);
		Button_2.setBackground(Color.WHITE);
		Button_2.setBounds(65, 12, 35, 35);
		panel_7.add(Button_2);
		
		Button_3.setHorizontalAlignment(SwingConstants.CENTER);
		Button_3.setBackground(Color.WHITE);
		Button_3.setBounds(114, 12, 35, 35);
		panel_7.add(Button_3);
		
		Button_4.setHorizontalAlignment(SwingConstants.CENTER);
		Button_4.setBackground(Color.WHITE);
		Button_4.setBounds(158, 12, 35, 35);
		panel_7.add(Button_4);
		
		Button_1.setHorizontalAlignment(SwingConstants.CENTER);
		Button_1.setBackground(Color.WHITE);
		Button_1.setBounds(16, 12, 35, 35);
		panel_7.add(Button_1);
		
		Button_5.setHorizontalAlignment(SwingConstants.CENTER);
		Button_5.setBackground(Color.WHITE);
		Button_5.setBounds(16, 64, 35, 35);
		panel_7.add(Button_5);
		
		Button_6.setHorizontalAlignment(SwingConstants.CENTER);
		Button_6.setBackground(Color.WHITE);
		Button_6.setBounds(65, 64, 35, 35);
		panel_7.add(Button_6);
		
		Button_7.setHorizontalAlignment(SwingConstants.CENTER);
		Button_7.setBackground(Color.WHITE);
		Button_7.setBounds(114, 64, 35, 35);
		panel_7.add(Button_7);
		
		Button_8.setHorizontalAlignment(SwingConstants.CENTER);
		Button_8.setBackground(Color.WHITE);
		Button_8.setBounds(158, 64, 35, 35);
		panel_7.add(Button_8);
		
		Field_5 = new JTextField();
		Field_5.setForeground(Color.WHITE);
		Field_5.setEnabled(false);
		Field_5.setEditable(false);
		Field_5.setColumns(10);
		Field_5.setBackground(Color.WHITE);
		Field_5.setBounds(207, 67, 14, 11);
		panel_7.add(Field_5);
		
		Field_7 = new JTextField();
		Field_7.setForeground(Color.WHITE);
		Field_7.setEnabled(false);
		Field_7.setEditable(false);
		Field_7.setColumns(10);
		Field_7.setBackground(Color.WHITE);
		Field_7.setBounds(207, 88, 14, 11);
		panel_7.add(Field_7);
		
		Field_6 = new JTextField();
		Field_6.setForeground(Color.WHITE);
		Field_6.setEnabled(false);
		Field_6.setEditable(false);
		Field_6.setColumns(10);
		Field_6.setBackground(Color.WHITE);
		Field_6.setBounds(231, 67, 14, 11);
		panel_7.add(Field_6);
		
		Field_8 = new JTextField();
		Field_8.setForeground(Color.WHITE);
		Field_8.setEnabled(false);
		Field_8.setEditable(false);
		Field_8.setColumns(10);
		Field_8.setBackground(Color.WHITE);
		Field_8.setBounds(231, 88, 14, 11);
		panel_7.add(Field_8);
		
		Field_10 = new JTextField();
		Field_10.setForeground(Color.WHITE);
		Field_10.setEnabled(false);
		Field_10.setEditable(false);
		Field_10.setColumns(10);
		Field_10.setBackground(Color.WHITE);
		Field_10.setBounds(231, 112, 14, 11);
		panel_7.add(Field_10);
		
		Field_12 = new JTextField();
		Field_12.setForeground(Color.WHITE);
		Field_12.setEnabled(false);
		Field_12.setEditable(false);
		Field_12.setColumns(10);
		Field_12.setBackground(Color.WHITE);
		Field_12.setBounds(231, 133, 14, 11);
		panel_7.add(Field_12);
		
		Field_11 = new JTextField();
		Field_11.setForeground(Color.WHITE);
		Field_11.setEnabled(false);
		Field_11.setEditable(false);
		Field_11.setColumns(10);
		Field_11.setBackground(Color.WHITE);
		Field_11.setBounds(207, 133, 14, 11);
		panel_7.add(Field_11);
		
		Field_9 = new JTextField();
		Field_9.setForeground(Color.WHITE);
		Field_9.setEnabled(false);
		Field_9.setEditable(false);
		Field_9.setColumns(10);
		Field_9.setBackground(Color.WHITE);
		Field_9.setBounds(207, 112, 14, 11);
		panel_7.add(Field_9);
		
		Button_12.setHorizontalAlignment(SwingConstants.CENTER);
		Button_12.setBackground(Color.WHITE);
		Button_12.setBounds(158, 112, 35, 35);
		panel_7.add(Button_12);
		
		Button_11.setHorizontalAlignment(SwingConstants.CENTER);
		Button_11.setBackground(Color.WHITE);
		Button_11.setBounds(114, 112, 35, 35);
		panel_7.add(Button_11);
		
		Button_10.setHorizontalAlignment(SwingConstants.CENTER);
		Button_10.setBackground(Color.WHITE);
		Button_10.setBounds(65, 112, 35, 35);
		panel_7.add(Button_10);
		
		Button_9.setHorizontalAlignment(SwingConstants.CENTER);
		Button_9.setBackground(Color.WHITE);
		Button_9.setBounds(16, 112, 35, 35);
		panel_7.add(Button_9);
		
		Button_13.setHorizontalAlignment(SwingConstants.CENTER);
		Button_13.setBackground(Color.WHITE);
		Button_13.setBounds(16, 156, 35, 35);
		panel_7.add(Button_13);
		
		Button_14.setHorizontalAlignment(SwingConstants.CENTER);
		Button_14.setBackground(Color.WHITE);
		Button_14.setBounds(65, 156, 35, 35);
		panel_7.add(Button_14);
		
		Button_15.setHorizontalAlignment(SwingConstants.CENTER);
		Button_15.setBackground(Color.WHITE);
		Button_15.setBounds(114, 156, 35, 35);
		panel_7.add(Button_15);
		
		Button_16.setHorizontalAlignment(SwingConstants.CENTER);
		Button_16.setBackground(Color.WHITE);
		Button_16.setBounds(158, 156, 35, 35);
		panel_7.add(Button_16);
		
		Field_13 = new JTextField();
		Field_13.setForeground(Color.WHITE);
		Field_13.setEnabled(false);
		Field_13.setEditable(false);
		Field_13.setColumns(10);
		Field_13.setBackground(Color.WHITE);
		Field_13.setBounds(207, 156, 14, 11);
		panel_7.add(Field_13);
		
		Field_15 = new JTextField();
		Field_15.setForeground(Color.WHITE);
		Field_15.setEnabled(false);
		Field_15.setEditable(false);
		Field_15.setColumns(10);
		Field_15.setBackground(Color.WHITE);
		Field_15.setBounds(207, 177, 14, 11);
		panel_7.add(Field_15);
		
		Field_14 = new JTextField();
		Field_14.setForeground(Color.WHITE);
		Field_14.setEnabled(false);
		Field_14.setEditable(false);
		Field_14.setColumns(10);
		Field_14.setBackground(Color.WHITE);
		Field_14.setBounds(231, 156, 14, 11);
		panel_7.add(Field_14);
		
		Field_16 = new JTextField();
		Field_16.setForeground(Color.WHITE);
		Field_16.setEnabled(false);
		Field_16.setEditable(false);
		Field_16.setColumns(10);
		Field_16.setBackground(Color.WHITE);
		Field_16.setBounds(231, 177, 14, 11);
		panel_7.add(Field_16);
		
		Button_17.setHorizontalAlignment(SwingConstants.CENTER);
		Button_17.setBackground(Color.WHITE);
		Button_17.setBounds(16, 204, 35, 35);
		panel_7.add(Button_17);
		
		Button_18.setHorizontalAlignment(SwingConstants.CENTER);
		Button_18.setBackground(Color.WHITE);
		Button_18.setBounds(65, 204, 35, 35);
		panel_7.add(Button_18);
	
		Button_19.setHorizontalAlignment(SwingConstants.CENTER);
		Button_19.setBackground(Color.WHITE);
		Button_19.setBounds(114, 204, 35, 35);
		panel_7.add(Button_19);
		
		Button_20.setHorizontalAlignment(SwingConstants.CENTER);
		Button_20.setBackground(Color.WHITE);
		Button_20.setBounds(158, 204, 35, 35);
		panel_7.add(Button_20);
		
		Field_17 = new JTextField();
		Field_17.setForeground(Color.WHITE);
		Field_17.setEnabled(false);
		Field_17.setEditable(false);
		Field_17.setColumns(10);
		Field_17.setBackground(Color.WHITE);
		Field_17.setBounds(207, 204, 14, 11);
		panel_7.add(Field_17);
		
		Field_19 = new JTextField();
		Field_19.setForeground(Color.WHITE);
		Field_19.setEnabled(false);
		Field_19.setEditable(false);
		Field_19.setColumns(10);
		Field_19.setBackground(Color.WHITE);
		Field_19.setBounds(207, 225, 14, 11);
		panel_7.add(Field_19);
		
		Field_20 = new JTextField();
		Field_20.setForeground(Color.WHITE);
		Field_20.setEnabled(false);
		Field_20.setEditable(false);
		Field_20.setColumns(10);
		Field_20.setBackground(Color.WHITE);
		Field_20.setBounds(231, 225, 14, 11);
		panel_7.add(Field_20);
		
		Field_18 = new JTextField();
		Field_18.setForeground(Color.WHITE);
		Field_18.setEnabled(false);
		Field_18.setEditable(false);
		Field_18.setColumns(10);
		Field_18.setBackground(Color.WHITE);
		Field_18.setBounds(231, 204, 14, 11);
		panel_7.add(Field_18);
		
		Field_22 = new JTextField();
		Field_22.setForeground(Color.WHITE);
		Field_22.setEnabled(false);
		Field_22.setEditable(false);
		Field_22.setColumns(10);
		Field_22.setBackground(Color.WHITE);
		Field_22.setBounds(231, 257, 14, 11);
		panel_7.add(Field_22);
		
		Field_24 = new JTextField();
		Field_24.setForeground(Color.WHITE);
		Field_24.setEnabled(false);
		Field_24.setEditable(false);
		Field_24.setColumns(10);
		Field_24.setBackground(Color.WHITE);
		Field_24.setBounds(231, 278, 14, 11);
		panel_7.add(Field_24);
		
		Field_23 = new JTextField();
		Field_23.setForeground(Color.WHITE);
		Field_23.setEnabled(false);
		Field_23.setEditable(false);
		Field_23.setColumns(10);
		Field_23.setBackground(Color.WHITE);
		Field_23.setBounds(207, 278, 14, 11);
		panel_7.add(Field_23);
		
		Field_21 = new JTextField();
		Field_21.setForeground(Color.WHITE);
		Field_21.setEnabled(false);
		Field_21.setEditable(false);
		Field_21.setColumns(10);
		Field_21.setBackground(Color.WHITE);
		Field_21.setBounds(207, 257, 14, 11);
		panel_7.add(Field_21);
		
		Button_24.setHorizontalAlignment(SwingConstants.CENTER);
		Button_24.setBackground(Color.WHITE);
		Button_24.setBounds(158, 257, 35, 35);
		panel_7.add(Button_24);
		
		Button_23.setHorizontalAlignment(SwingConstants.CENTER);
		Button_23.setBackground(Color.WHITE);
		Button_23.setBounds(114, 257, 35, 35);
		panel_7.add(Button_23);
		
		Button_22.setHorizontalAlignment(SwingConstants.CENTER);
		Button_22.setBackground(Color.WHITE);
		Button_22.setBounds(65, 257, 35, 35);
		panel_7.add(Button_22);
		
		
		Button_21.setHorizontalAlignment(SwingConstants.CENTER);
		Button_21.setBackground(Color.WHITE);
		Button_21.setBounds(16, 257, 35, 35);
		panel_7.add(Button_21);
		
		JLabel label = new JLabel("Boje:\r");
		label.setFont(new Font("Arial", Font.BOLD, 13));
		label.setBounds(10, 6, 46, 23);
		panel_6.add(label);
		
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Button_1.isSelected())
				{
					Button_1.setBackground(Color.RED);	
				}
				if(Button_2.isSelected())
				{
					Button_2.setBackground(Color.RED);	
				}
				if(Button_3.isSelected())
				{
					Button_3.setBackground(Color.RED);	
				}
				if(Button_4.isSelected())
				{
					Button_4.setBackground(Color.RED);	
				}
				if(Button_5.isSelected())
				{
					Button_5.setBackground(Color.RED);	
				}
				if(Button_6.isSelected())
				{
					Button_6.setBackground(Color.RED);	
				}
				if(Button_7.isSelected())
				{
					Button_7.setBackground(Color.RED);	
				}
				if(Button_8.isSelected())
				{
					Button_8.setBackground(Color.RED);	
				}
				if(Button_9.isSelected())
				{
					Button_9.setBackground(Color.RED);	
				}
				if(Button_10.isSelected())
				{
					Button_10.setBackground(Color.RED);	
				}
				if(Button_11.isSelected())
				{
					Button_11.setBackground(Color.RED);	
				}
				if(Button_12.isSelected())
				{
					Button_12.setBackground(Color.RED);	
				}
				if(Button_13.isSelected())
				{
					Button_13.setBackground(Color.RED);	
				}
				if(Button_14.isSelected())
				{
					Button_14.setBackground(Color.RED);	
				}
				if(Button_15.isSelected())
				{
					Button_15.setBackground(Color.RED);	
				}
				if(Button_16.isSelected())
				{
					Button_16.setBackground(Color.RED);	
				}
				if(Button_17.isSelected())
				{
					Button_17.setBackground(Color.RED);	
				}
				if(Button_18.isSelected())
				{
					Button_18.setBackground(Color.RED);	
				}
				if(Button_19.isSelected())
				{
					Button_19.setBackground(Color.RED);	
				}
				if(Button_20.isSelected())
				{
					Button_20.setBackground(Color.RED);	
				}
				if(Button_21.isSelected())
				{
					Button_21.setBackground(Color.RED);	
				}
				if(Button_22.isSelected())
				{
					Button_22.setBackground(Color.RED);	
				}
				if(Button_23.isSelected())
				{
					Button_23.setBackground(Color.RED);	
				}
				if(Button_24.isSelected())
				{
					Button_24.setBackground(Color.RED);	
				}
			}
		});
		button.setBackground(Color.RED);
		button.setBounds(55, 6, 40, 33);
		panel_6.add(button);
		
		JButton button_1 = new JButton("");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Button_1.isSelected())
				{
					Button_1.setBackground(Color.ORANGE);	
				}
				if(Button_2.isSelected())
				{
					Button_2.setBackground(Color.ORANGE);	
				}
				if(Button_3.isSelected())
				{
					Button_3.setBackground(Color.ORANGE);	
				}
				if(Button_4.isSelected())
				{
					Button_4.setBackground(Color.ORANGE);	
				}
				if(Button_5.isSelected())
				{
					Button_5.setBackground(Color.ORANGE);	
				}
				if(Button_6.isSelected())
				{
					Button_6.setBackground(Color.ORANGE);	
				}
				if(Button_7.isSelected())
				{
					Button_7.setBackground(Color.ORANGE);	
				}
				if(Button_8.isSelected())
				{
					Button_8.setBackground(Color.ORANGE);	
				}
				if(Button_9.isSelected())
				{
					Button_9.setBackground(Color.ORANGE);	
				}
				if(Button_10.isSelected())
				{
					Button_10.setBackground(Color.ORANGE);	
				}
				if(Button_11.isSelected())
				{
					Button_11.setBackground(Color.ORANGE);	
				}
				if(Button_12.isSelected())
				{
					Button_12.setBackground(Color.ORANGE);	
				}
				if(Button_13.isSelected())
				{
					Button_13.setBackground(Color.ORANGE);	
				}
				if(Button_14.isSelected())
				{
					Button_14.setBackground(Color.ORANGE);	
				}
				if(Button_15.isSelected())
				{
					Button_15.setBackground(Color.ORANGE);	
				}
				if(Button_16.isSelected())
				{
					Button_16.setBackground(Color.ORANGE);	
				}
				if(Button_17.isSelected())
				{
					Button_17.setBackground(Color.ORANGE);	
				}
				if(Button_18.isSelected())
				{
					Button_18.setBackground(Color.ORANGE);	
				}
				if(Button_19.isSelected())
				{
					Button_19.setBackground(Color.ORANGE);	
				}
				if(Button_20.isSelected())
				{
					Button_20.setBackground(Color.ORANGE);	
				}
				if(Button_21.isSelected())
				{
					Button_21.setBackground(Color.ORANGE);	
				}
				if(Button_22.isSelected())
				{
					Button_22.setBackground(Color.ORANGE);	
				}
				if(Button_23.isSelected())
				{
					Button_23.setBackground(Color.ORANGE);	
				}
				if(Button_24.isSelected())
				{
					Button_24.setBackground(Color.ORANGE);	
				}
			}
		});
		button_1.setBackground(Color.ORANGE);
		button_1.setBounds(105, 6, 40, 33);
		panel_6.add(button_1);
		
		JButton button_2 = new JButton("");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Button_1.isSelected())
				{
					Button_1.setBackground(Color.GREEN);	
				}
				if(Button_2.isSelected())
				{
					Button_2.setBackground(Color.GREEN);	
				}
				if(Button_3.isSelected())
				{
					Button_3.setBackground(Color.GREEN);	
				}
				if(Button_4.isSelected())
				{
					Button_4.setBackground(Color.GREEN);	
				}
				if(Button_5.isSelected())
				{
					Button_5.setBackground(Color.GREEN);	
				}
				if(Button_6.isSelected())
				{
					Button_6.setBackground(Color.GREEN);	
				}
				if(Button_7.isSelected())
				{
					Button_7.setBackground(Color.GREEN);	
				}
				if(Button_8.isSelected())
				{
					Button_8.setBackground(Color.GREEN);	
				}
				if(Button_9.isSelected())
				{
					Button_9.setBackground(Color.GREEN);	
				}
				if(Button_10.isSelected())
				{
					Button_10.setBackground(Color.GREEN);	
				}
				if(Button_11.isSelected())
				{
					Button_11.setBackground(Color.GREEN);	
				}
				if(Button_12.isSelected())
				{
					Button_12.setBackground(Color.GREEN);	
				}
				if(Button_13.isSelected())
				{
					Button_13.setBackground(Color.GREEN);	
				}
				if(Button_14.isSelected())
				{
					Button_14.setBackground(Color.GREEN);	
				}
				if(Button_15.isSelected())
				{
					Button_15.setBackground(Color.GREEN);	
				}
				if(Button_16.isSelected())
				{
					Button_16.setBackground(Color.GREEN);	
				}
				if(Button_17.isSelected())
				{
					Button_17.setBackground(Color.GREEN);	
				}
				if(Button_18.isSelected())
				{
					Button_18.setBackground(Color.GREEN);	
				}
				if(Button_19.isSelected())
				{
					Button_19.setBackground(Color.GREEN);	
				}
				if(Button_20.isSelected())
				{
					Button_20.setBackground(Color.GREEN);	
				}
				if(Button_21.isSelected())
				{
					Button_21.setBackground(Color.GREEN);	
				}
				if(Button_22.isSelected())
				{
					Button_22.setBackground(Color.GREEN);	
				}
				if(Button_23.isSelected())
				{
					Button_23.setBackground(Color.GREEN);	
				}
				if(Button_24.isSelected())
				{
					Button_24.setBackground(Color.GREEN);	
				}
			}
		});
		button_2.setBackground(Color.GREEN);
		button_2.setBounds(155, 6, 40, 33);
		panel_6.add(button_2);
		
		JButton button_3 = new JButton("");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Button_1.isSelected())
				{
					Button_1.setBackground(Color.CYAN);	
				}
				if(Button_2.isSelected())
				{
					Button_2.setBackground(Color.CYAN);	
				}
				if(Button_3.isSelected())
				{
					Button_3.setBackground(Color.CYAN);	
				}
				if(Button_4.isSelected())
				{
					Button_4.setBackground(Color.CYAN);	
				}
				if(Button_5.isSelected())
				{
					Button_5.setBackground(Color.CYAN);	
				}
				if(Button_6.isSelected())
				{
					Button_6.setBackground(Color.CYAN);	
				}
				if(Button_7.isSelected())
				{
					Button_7.setBackground(Color.CYAN);	
				}
				if(Button_8.isSelected())
				{
					Button_8.setBackground(Color.CYAN);	
				}
				if(Button_9.isSelected())
				{
					Button_9.setBackground(Color.CYAN);	
				}
				if(Button_10.isSelected())
				{
					Button_10.setBackground(Color.CYAN);	
				}
				if(Button_11.isSelected())
				{
					Button_11.setBackground(Color.CYAN);	
				}
				if(Button_12.isSelected())
				{
					Button_12.setBackground(Color.CYAN);	
				}
				if(Button_13.isSelected())
				{
					Button_13.setBackground(Color.CYAN);	
				}
				if(Button_14.isSelected())
				{
					Button_14.setBackground(Color.CYAN);	
				}
				if(Button_15.isSelected())
				{
					Button_15.setBackground(Color.CYAN);	
				}
				if(Button_16.isSelected())
				{
					Button_16.setBackground(Color.CYAN);	
				}
				if(Button_17.isSelected())
				{
					Button_17.setBackground(Color.CYAN);	
				}
				if(Button_18.isSelected())
				{
					Button_18.setBackground(Color.CYAN);	
				}
				if(Button_19.isSelected())
				{
					Button_19.setBackground(Color.CYAN);	
				}
				if(Button_20.isSelected())
				{
					Button_20.setBackground(Color.CYAN);	
				}
				if(Button_21.isSelected())
				{
					Button_21.setBackground(Color.CYAN);	
				}
				if(Button_22.isSelected())
				{
					Button_22.setBackground(Color.CYAN);	
				}
				if(Button_23.isSelected())
				{
					Button_23.setBackground(Color.CYAN);	
				}
				if(Button_24.isSelected())
				{
					Button_24.setBackground(Color.CYAN);	
				}
			}
		});
		button_3.setBackground(Color.CYAN);
		button_3.setBounds(205, 6, 40, 33);
		panel_6.add(button_3);
		
		JButton button_4 = new JButton("");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Button_1.isSelected())
				{
					Button_1.setBackground(Color.YELLOW);	
				}
				if(Button_2.isSelected())
				{
					Button_2.setBackground(Color.YELLOW);	
				}
				if(Button_3.isSelected())
				{
					Button_3.setBackground(Color.YELLOW);	
				}
				if(Button_4.isSelected())
				{
					Button_4.setBackground(Color.YELLOW);	
				}
				if(Button_5.isSelected())
				{
					Button_5.setBackground(Color.YELLOW);	
				}
				if(Button_6.isSelected())
				{
					Button_6.setBackground(Color.YELLOW);	
				}
				if(Button_7.isSelected())
				{
					Button_7.setBackground(Color.YELLOW);	
				}
				if(Button_8.isSelected())
				{
					Button_8.setBackground(Color.YELLOW);	
				}
				if(Button_9.isSelected())
				{
					Button_9.setBackground(Color.YELLOW);	
				}
				if(Button_10.isSelected())
				{
					Button_10.setBackground(Color.YELLOW);	
				}
				if(Button_11.isSelected())
				{
					Button_11.setBackground(Color.YELLOW);	
				}
				if(Button_12.isSelected())
				{
					Button_12.setBackground(Color.YELLOW);	
				}
				if(Button_13.isSelected())
				{
					Button_13.setBackground(Color.YELLOW);	
				}
				if(Button_14.isSelected())
				{
					Button_14.setBackground(Color.YELLOW);	
				}
				if(Button_15.isSelected())
				{
					Button_15.setBackground(Color.YELLOW);	
				}
				if(Button_16.isSelected())
				{
					Button_16.setBackground(Color.YELLOW);	
				}
				if(Button_17.isSelected())
				{
					Button_17.setBackground(Color.YELLOW);	
				}
				if(Button_18.isSelected())
				{
					Button_18.setBackground(Color.YELLOW);	
				}
				if(Button_19.isSelected())
				{
					Button_19.setBackground(Color.YELLOW);	
				}
				if(Button_20.isSelected())
				{
					Button_20.setBackground(Color.YELLOW);	
				}
				if(Button_21.isSelected())
				{
					Button_21.setBackground(Color.YELLOW);	
				}
				if(Button_22.isSelected())
				{
					Button_22.setBackground(Color.YELLOW);	
				}
				if(Button_23.isSelected())
				{
					Button_23.setBackground(Color.YELLOW);	
				}
				if(Button_24.isSelected())
				{
					Button_24.setBackground(Color.YELLOW);	
				}
			}
		});
		button_4.setBackground(Color.YELLOW);
		button_4.setBounds(255, 6, 40, 33);
		panel_6.add(button_4);
		
		JButton button_5 = new JButton("");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(Button_1.isSelected())
				{
					Button_1.setBackground(Color.PINK);	
				}
				if(Button_2.isSelected())
				{
					Button_2.setBackground(Color.PINK);	
				}
				if(Button_3.isSelected())
				{
					Button_3.setBackground(Color.PINK);	
				}
				if(Button_4.isSelected())
				{
					Button_4.setBackground(Color.PINK);	
				}
				if(Button_5.isSelected())
				{
					Button_5.setBackground(Color.PINK);	
				}
				if(Button_6.isSelected())
				{
					Button_6.setBackground(Color.PINK);	
				}
				if(Button_7.isSelected())
				{
					Button_7.setBackground(Color.PINK);	
				}
				if(Button_8.isSelected())
				{
					Button_8.setBackground(Color.PINK);	
				}
				if(Button_9.isSelected())
				{
					Button_9.setBackground(Color.PINK);	
				}
				if(Button_10.isSelected())
				{
					Button_10.setBackground(Color.PINK);	
				}
				if(Button_11.isSelected())
				{
					Button_11.setBackground(Color.PINK);	
				}
				if(Button_12.isSelected())
				{
					Button_12.setBackground(Color.PINK);	
				}
				if(Button_13.isSelected())
				{
					Button_13.setBackground(Color.PINK);	
				}
				if(Button_14.isSelected())
				{
					Button_14.setBackground(Color.PINK);	
				}
				if(Button_15.isSelected())
				{
					Button_15.setBackground(Color.PINK);	
				}
				if(Button_16.isSelected())
				{
					Button_16.setBackground(Color.PINK);	
				}
				if(Button_17.isSelected())
				{
					Button_17.setBackground(Color.PINK);	
				}
				if(Button_18.isSelected())
				{
					Button_18.setBackground(Color.PINK);	
				}
				if(Button_19.isSelected())
				{
					Button_19.setBackground(Color.PINK);	
				}
				if(Button_20.isSelected())
				{
					Button_20.setBackground(Color.PINK);	
				}
				if(Button_21.isSelected())
				{
					Button_21.setBackground(Color.PINK);	
				}
				if(Button_22.isSelected())
				{
					Button_22.setBackground(Color.PINK);	
				}
				if(Button_23.isSelected())
				{
					Button_23.setBackground(Color.PINK);	
				}
				if(Button_24.isSelected())
				{
					Button_24.setBackground(Color.PINK);	
				}
			}
		});
		button_5.setBackground(Color.PINK);
		button_5.setBounds(305, 6, 40, 33);
		panel_6.add(button_5);
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(Field_30.getBackground().equals(Button_1.getBackground()))
					Field_1.setBackground(Color.RED);
					else if(Field_30.getBackground().equals(Button_2.getBackground()))
						Field_1.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_3.getBackground()))
						Field_1.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_4.getBackground()))
						Field_1.setBackground(Color.black);
					else 
						Field_1.setBackground(Color.white);
				
				if(Field_31.getBackground().equals(Button_1.getBackground()))
				Field_2.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_2.getBackground()))
					Field_2.setBackground(Color.RED);
				else if(Field_31.getBackground().equals(Button_3.getBackground()))
					Field_2.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_4.getBackground()))
					Field_2.setBackground(Color.black);
				else 
					Field_2.setBackground(Color.white);
				
				if(Field_32.getBackground().equals(Button_1.getBackground()))
					Field_3.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_2.getBackground()))
						Field_3.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_3.getBackground()))
						Field_3.setBackground(Color.RED);
					else if(Field_32.getBackground().equals(Button_4.getBackground()))
						Field_3.setBackground(Color.black);
					else 
						Field_3.setBackground(Color.white);
				
				if(Field_33.getBackground().equals(Button_1.getBackground()))
					Field_4.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_2.getBackground()))
						Field_4.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_3.getBackground()))
						Field_4.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_4.getBackground()))
						Field_4.setBackground(Color.RED);
					else 
						Field_4.setBackground(Color.white);
				
				if(Field_30.getBackground().equals(Button_1.getBackground())&&Field_31.getBackground().equals(Button_2.getBackground())&&Field_32.getBackground().equals(Button_3.getBackground())&&Field_33.getBackground().equals(Button_4.getBackground()))
					{JOptionPane.showMessageDialog(null, "Bravo pogodili ste kombinaciju!!! ");
					Field_1.setBackground(Color.red);
					Field_2.setBackground(Color.red);
					Field_3.setBackground(Color.red);
					Field_4.setBackground(Color.red);
					brB=brB+1000;
					Field_29.setText(" Osvojili ste "+brB+ "din");
					}
			}
		});
		button_6.setFont(new Font("Arial", Font.BOLD, 12));
		button_6.setBackground(new Color(60, 179, 113));
		button_6.setBounds(279, 62, 95, 23);
		
		panel_6.add(button_6);
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Field_30.getBackground().equals(Button_5.getBackground()))
					Field_5.setBackground(Color.RED);
					else if(Field_30.getBackground().equals(Button_6.getBackground()))
						Field_5.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_7.getBackground()))
						Field_5.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_8.getBackground()))
						Field_5.setBackground(Color.black);
					else 
						Field_5.setBackground(Color.white);
				
				if(Field_31.getBackground().equals(Button_5.getBackground()))
					Field_6.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_6.getBackground()))
					Field_6.setBackground(Color.RED);
				else if(Field_31.getBackground().equals(Button_7.getBackground()))
					Field_6.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_8.getBackground()))
					Field_6.setBackground(Color.black);
				else 
					Field_6.setBackground(Color.white);
				
				if(Field_32.getBackground().equals(Button_5.getBackground()))
					Field_7.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_6.getBackground()))
						Field_7.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_7.getBackground()))
						Field_7.setBackground(Color.RED);
					else if(Field_32.getBackground().equals(Button_8.getBackground()))
						Field_7.setBackground(Color.black);
					else 
						Field_7.setBackground(Color.white);
				
				if(Field_33.getBackground().equals(Button_5.getBackground()))
					Field_8.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_6.getBackground()))
						Field_8.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_7.getBackground()))
						Field_8.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_8.getBackground()))
						Field_8.setBackground(Color.RED);
					else 
						Field_8.setBackground(Color.white);
				
				if(Field_30.getBackground().equals(Button_5.getBackground())&&Field_31.getBackground().equals(Button_6.getBackground())&&Field_32.getBackground().equals(Button_7.getBackground())&&Field_33.getBackground().equals(Button_8.getBackground()))
					{JOptionPane.showMessageDialog(null, "Bravo pogodili ste kombinaciju!!! ");
					Field_5.setBackground(Color.red);
					Field_6.setBackground(Color.red);
					Field_7.setBackground(Color.red);
					Field_8.setBackground(Color.red);
					brB=brB+800;
					Field_29.setText(" Osvojili ste "+brB+ "din");
				
					}
			}
		});
		button_7.setFont(new Font("Arial", Font.BOLD, 12));
		button_7.setBackground(new Color(60, 179, 113));
		button_7.setBounds(277, 117, 97, 23);
		
		panel_6.add(button_7);
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Field_30.getBackground().equals(Button_9.getBackground()))
					Field_9.setBackground(Color.RED);
					else if(Field_30.getBackground().equals(Button_10.getBackground()))
						Field_9.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_11.getBackground()))
						Field_9.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_12.getBackground()))
						Field_9.setBackground(Color.black);
					else 
						Field_9.setBackground(Color.white);
				
				if(Field_31.getBackground().equals(Button_9.getBackground()))
					Field_10.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_10.getBackground()))
					Field_10.setBackground(Color.RED);
				else if(Field_31.getBackground().equals(Button_11.getBackground()))
					Field_10.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_12.getBackground()))
					Field_10.setBackground(Color.black);
				else 
					Field_10.setBackground(Color.white);
				
				if(Field_32.getBackground().equals(Button_9.getBackground()))
					Field_11.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_10.getBackground()))
						Field_11.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_11.getBackground()))
						Field_11.setBackground(Color.RED);
					else if(Field_32.getBackground().equals(Button_12.getBackground()))
						Field_11.setBackground(Color.black);
					else 
						Field_11.setBackground(Color.white);
				
				if(Field_33.getBackground().equals(Button_9.getBackground()))
					Field_12.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_10.getBackground()))
						Field_12.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_11.getBackground()))
						Field_12.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_12.getBackground()))
						Field_12.setBackground(Color.RED);
					else 
						Field_12.setBackground(Color.white);
				
				if(Field_30.getBackground().equals(Button_9.getBackground())&&Field_31.getBackground().equals(Button_10.getBackground())&&Field_32.getBackground().equals(Button_11.getBackground())&&Field_33.getBackground().equals(Button_12.getBackground()))
					{JOptionPane.showMessageDialog(null, "Bravo pogodili ste kombinaciju!!! ");
					Field_9.setBackground(Color.red);
					Field_10.setBackground(Color.red);
					Field_11.setBackground(Color.red);
					Field_12.setBackground(Color.red);
					brB=brB+600;
					Field_29.setText(" Osvojili ste "+brB+ "din");
					}
			}
		});
		button_8.setFont(new Font("Arial", Font.BOLD, 12));
		button_8.setBackground(new Color(60, 179, 113));
		button_8.setBounds(277, 163, 97, 23);
		
		panel_6.add(button_8);
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Field_30.getBackground().equals(Button_13.getBackground()))
					Field_13.setBackground(Color.RED);
					else if(Field_30.getBackground().equals(Button_14.getBackground()))
						Field_13.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_15.getBackground()))
						Field_13.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_16.getBackground()))
						Field_13.setBackground(Color.black);
					else 
						Field_13.setBackground(Color.white);
				
				if(Field_31.getBackground().equals(Button_13.getBackground()))
					Field_14.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_14.getBackground()))
					Field_14.setBackground(Color.RED);
				else if(Field_31.getBackground().equals(Button_15.getBackground()))
					Field_14.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_16.getBackground()))
					Field_14.setBackground(Color.black);
				else 
					Field_14.setBackground(Color.white);
				
				if(Field_32.getBackground().equals(Button_13.getBackground()))
					Field_15.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_14.getBackground()))
						Field_15.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_15.getBackground()))
						Field_15.setBackground(Color.RED);
					else if(Field_32.getBackground().equals(Button_16.getBackground()))
						Field_15.setBackground(Color.black);
					else 
						Field_15.setBackground(Color.white);
				
				if(Field_33.getBackground().equals(Button_13.getBackground()))
					Field_16.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_14.getBackground()))
						Field_16.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_15.getBackground()))
						Field_16.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_16.getBackground()))
						Field_16.setBackground(Color.RED);
					else 
						Field_16.setBackground(Color.white);
				
				if(Field_30.getBackground().equals(Button_13.getBackground())&&Field_31.getBackground().equals(Button_14.getBackground())&&Field_32.getBackground().equals(Button_15.getBackground())&&Field_33.getBackground().equals(Button_16.getBackground()))
					{JOptionPane.showMessageDialog(null, "Bravo pogodili ste kombinaciju!!! ");
					Field_13.setBackground(Color.red);
					Field_14.setBackground(Color.red);
					Field_15.setBackground(Color.red);
					Field_16.setBackground(Color.red);
					brB=brB+400;
					Field_29.setText(" Osvojili ste "+brB+ "din");
					}
			}
		});
		button_9.setFont(new Font("Arial", Font.BOLD, 12));
		button_9.setBackground(new Color(60, 179, 113));
		button_9.setBounds(277, 208, 97, 23);
		
		panel_6.add(button_9);
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Field_30.getBackground().equals(Button_17.getBackground()))
					Field_17.setBackground(Color.RED);
					else if(Field_30.getBackground().equals(Button_18.getBackground()))
						Field_17.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_19.getBackground()))
						Field_17.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_20.getBackground()))
						Field_17.setBackground(Color.black);
					else 
						Field_17.setBackground(Color.white);
				
				if(Field_31.getBackground().equals(Button_17.getBackground()))
					Field_18.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_18.getBackground()))
					Field_18.setBackground(Color.RED);
				else if(Field_31.getBackground().equals(Button_19.getBackground()))
					Field_18.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_20.getBackground()))
					Field_18.setBackground(Color.black);
				else 
					Field_18.setBackground(Color.white);
				
				if(Field_32.getBackground().equals(Button_17.getBackground()))
					Field_19.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_18.getBackground()))
						Field_19.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_19.getBackground()))
						Field_19.setBackground(Color.RED);
					else if(Field_32.getBackground().equals(Button_20.getBackground()))
						Field_19.setBackground(Color.black);
					else 
						Field_19.setBackground(Color.white);
				
				if(Field_33.getBackground().equals(Button_17.getBackground()))
					Field_20.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_18.getBackground()))
						Field_20.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_19.getBackground()))
						Field_20.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_20.getBackground()))
						Field_20.setBackground(Color.RED);
					else 
						Field_20.setBackground(Color.white);
				
				if(Field_30.getBackground().equals(Field_17.getBackground())&&Field_31.getBackground().equals(Field_18.getBackground())&&Field_32.getBackground().equals(Field_19.getBackground())&&Field_33.getBackground().equals(Field_20.getBackground()))
					{JOptionPane.showMessageDialog(null, "Bravo pogodili ste kombinaciju!!! ");
					Field_17.setBackground(Color.red);
					Field_18.setBackground(Color.red);
					Field_19.setBackground(Color.red);
					Field_20.setBackground(Color.red);
					brB=brB+300;
					Field_29.setText(" Osvojili ste "+brB+ "din");
					}
			}
		});
		button_10.setFont(new Font("Arial", Font.BOLD, 12));
		button_10.setBackground(new Color(60, 179, 113));
		button_10.setBounds(277, 254, 97, 23);
		
		panel_6.add(button_10);
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Field_30.getBackground().equals(Button_21.getBackground()))
					Field_21.setBackground(Color.RED);
					else if(Field_30.getBackground().equals(Button_22.getBackground()))
						Field_21.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_23.getBackground()))
						Field_21.setBackground(Color.black);
					else if(Field_30.getBackground().equals(Button_24.getBackground()))
						Field_21.setBackground(Color.black);
					else 
						Field_21.setBackground(Color.white);
				
				if(Field_31.getBackground().equals(Button_21.getBackground()))
					Field_22.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_22.getBackground()))
					Field_22.setBackground(Color.RED);
				else if(Field_31.getBackground().equals(Button_23.getBackground()))
					Field_22.setBackground(Color.black);
				else if(Field_31.getBackground().equals(Button_24.getBackground()))
					Field_22.setBackground(Color.black);
				else 
					Field_22.setBackground(Color.white);
				
				if(Field_32.getBackground().equals(Button_21.getBackground()))
					Field_23.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_22.getBackground()))
						Field_23.setBackground(Color.black);
					else if(Field_32.getBackground().equals(Button_23.getBackground()))
						Field_23.setBackground(Color.RED);
					else if(Field_32.getBackground().equals(Button_24.getBackground()))
						Field_23.setBackground(Color.black);
					else 
						Field_19.setBackground(Color.white);
				
				if(Field_33.getBackground().equals(Button_21.getBackground()))
					Field_24.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_22.getBackground()))
						Field_24.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_23.getBackground()))
						Field_24.setBackground(Color.black);
					else if(Field_33.getBackground().equals(Button_24.getBackground()))
						Field_24.setBackground(Color.RED);
					else 
						Field_24.setBackground(Color.white);
				
				if(Field_30.getBackground().equals(Button_21.getBackground())&&Field_31.getBackground().equals(Button_22.getBackground())&&Field_32.getBackground().equals(Button_23.getBackground())&&Field_33.getBackground().equals(Button_24.getBackground()))
					{JOptionPane.showMessageDialog(null, "Bravo pogodili ste kombinaciju!!! ");
					Field_21.setBackground(Color.red);
					Field_22.setBackground(Color.red);
					Field_23.setBackground(Color.red);
					Field_24.setBackground(Color.red);
					brB=brB+100;
					Field_29.setText(" Osvojili ste "+brB+ "din");
					}
					else
					{Field_25.setBackground(Field_30.getBackground());
					Field_26.setBackground(Field_31.getBackground());
					Field_27.setBackground(Field_32.getBackground());
					Field_28.setBackground(Field_33.getBackground());
					}
			}
		});
		button_11.setFont(new Font("Arial", Font.BOLD, 12));
		button_11.setBackground(new Color(60, 179, 113));
		button_11.setBounds(277, 306, 97, 23);
		
		panel_6.add(button_11);
		lblOsvojenaSuma.setFont(new Font("Arial", Font.BOLD, 13));
		lblOsvojenaSuma.setBounds(429, 163, 116, 23);
		
		panel_6.add(lblOsvojenaSuma);
		Field_29.setFont(new Font("Arial", Font.BOLD, 12));
		Field_29.setColumns(10);
		Field_29.setBounds(396, 188, 165, 40);
		
		panel_6.add(Field_29);
		panel_8.setLayout(null);
		panel_8.setBackground(new Color(218, 165, 32));
		panel_8.setBounds(355, 6, 46, 33);
		
		panel_6.add(panel_8);
		Field_30.setEnabled(false);
		Field_30.setEditable(false);
		Field_30.setColumns(10);
		Field_30.setBackground(Color.WHITE);
		Field_30.setBounds(10, 76, 23, 20);
		
		panel_8.add(Field_30);
		Field_31.setEnabled(false);
		Field_31.setEditable(false);
		Field_31.setColumns(10);
		Field_31.setBackground(Color.WHITE);
		Field_31.setBounds(43, 76, 23, 20);
		
		panel_8.add(Field_31);
		Field_32.setEnabled(false);
		Field_32.setEditable(false);
		Field_32.setColumns(10);
		Field_32.setBackground(Color.WHITE);
		Field_32.setBounds(76, 76, 23, 20);
		
		panel_8.add(Field_32);
		Field_33.setEnabled(false);
		Field_33.setEditable(false);
		Field_33.setColumns(10);
		Field_33.setBackground(Color.WHITE);
		Field_33.setBounds(111, 76, 23, 20);
		
		panel_8.add(Field_33);
		label_7.setFont(new Font("Arial", Font.BOLD, 13));
		label_7.setBounds(447, 278, 61, 18);
		
		panel_6.add(label_7);
		Field_25.setEnabled(false);
		Field_25.setEditable(false);
		Field_25.setColumns(10);
		Field_25.setBackground(Color.WHITE);
		Field_25.setBounds(419, 307, 22, 22);
		
		panel_6.add(Field_25);
		Field_26.setEnabled(false);
		Field_26.setEditable(false);
		Field_26.setColumns(10);
		Field_26.setBackground(Color.WHITE);
		Field_26.setBounds(451, 307, 22, 22);
		
		panel_6.add(Field_26);
		Field_27.setEnabled(false);
		Field_27.setEditable(false);
		Field_27.setColumns(10);
		Field_27.setBackground(Color.WHITE);
		Field_27.setBounds(481, 307, 22, 22);
		
		panel_6.add(Field_27);
		Field_28.setEnabled(false);
		Field_28.setEditable(false);
		Field_28.setColumns(10);
		Field_28.setBackground(Color.WHITE);
		Field_28.setBounds(513, 307, 22, 22);
		
		panel_6.add(Field_28);
		btnKombinacija.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int boja = new Random().nextInt(8);
				
				switch (boja) {
				case 1:
					Field_30.setBackground(Color.RED);
					
					break;
				case 2:
					Field_30.setBackground(Color.CYAN);
					

					break;
				case 3:
					Field_30.setBackground(Color.YELLOW);
					
					break;
				case 4:
					Field_30.setBackground(Color.GREEN);
					
					break;
				case 5:
					Field_30.setBackground(Color.PINK);
					
					break;
				default:
					Field_30.setBackground(Color.ORANGE);
					
					break;
				}
				//-------------------------------------------------------------------------
				switch (boja) {
				case 1:
					Field_31.setBackground(Color.GREEN);
					
					break;
				case 2:
					Field_31.setBackground(Color.YELLOW);
					

					break;
				case 3:
					Field_31.setBackground(Color.RED);
					
					break;
				case 4:
					Field_31.setBackground(Color.ORANGE);
					
					break;
				case 5:
					Field_31.setBackground(Color.PINK);
					
					break;
				case 6:
					Field_31.setBackground(Color.CYAN);
					
					break;
				case 7:
					Field_31.setBackground(Color.YELLOW);
					
					break;
				default:
					Field_31.setBackground(Color.CYAN);
					
					break;
				}
				//-------------------------------------------------------------------------
				switch (boja) {
				case 1:
					Field_32.setBackground(Color.YELLOW);
					
					break;
				case 2:
					Field_32.setBackground(Color.RED);
					

					break;
				case 3:
					Field_32.setBackground(Color.GREEN);
					
					break;
				case 4:
					Field_32.setBackground(Color.ORANGE);
					
					break;
				case 5:
					Field_32.setBackground(Color.PINK);
					
					break;
				case 6:
					Field_32.setBackground(Color.GREEN);
					
					break;
				case 7:
					Field_32.setBackground(Color.RED);
					
					break;
				default:
					Field_32.setBackground(Color.CYAN);
					
					break;
				}
				//-------------------------------------------------------------------------
				switch (boja) {
				case 1:
					Field_33.setBackground(Color.GREEN);
					
					break;
				case 2:
					Field_33.setBackground(Color.ORANGE);
					

					break;
				case 3:
					Field_33.setBackground(Color.PINK);
					
					break;
				case 4:
					Field_33.setBackground(Color.CYAN);
					
					break;
				case 5:
					Field_33.setBackground(Color.YELLOW);
					
					break;
				case 6:
					Field_33.setBackground(Color.PINK);
					
					break;
				case 7:
					Field_33.setBackground(Color.ORANGE);
					
					break;
				default:
					Field_33.setBackground(Color.RED);
					
					break;
				}
					Field_29.setText(null);
				
				Button_1.setBackground(Color.white);	
				Button_2.setBackground(Color.white);	
				Button_3.setBackground(Color.white);
				Button_4.setBackground(Color.white);
				Button_5.setBackground(Color.white);
				Button_6.setBackground(Color.white);
				Button_7.setBackground(Color.white);
				Button_8.setBackground(Color.white);
				Button_9.setBackground(Color.white);
				Button_10.setBackground(Color.white);
				Button_11.setBackground(Color.white);
				Button_12.setBackground(Color.white);
				Button_13.setBackground(Color.white);
				Button_14.setBackground(Color.white);
				Button_15.setBackground(Color.white);
				Button_16.setBackground(Color.white);
				Button_17.setBackground(Color.white);
				Button_18.setBackground(Color.white);
				Button_19.setBackground(Color.white);
				Button_20.setBackground(Color.white);
				Button_21.setBackground(Color.white);
				Button_22.setBackground(Color.white);
				Button_23.setBackground(Color.white);
				Button_24.setBackground(Color.white);
				Field_1.setBackground(Color.white);
				Field_2.setBackground(Color.white);
				Field_3.setBackground(Color.white);
				Field_4.setBackground(Color.white);
				Field_5.setBackground(Color.white);
				Field_6.setBackground(Color.white);
				Field_7.setBackground(Color.white);
				Field_8.setBackground(Color.white);
				Field_9.setBackground(Color.white);
				Field_10.setBackground(Color.white);
				Field_11.setBackground(Color.white);
				Field_12.setBackground(Color.white);
				Field_13.setBackground(Color.white);
				Field_14.setBackground(Color.white);
				Field_15.setBackground(Color.white);
				Field_16.setBackground(Color.white);
				Field_17.setBackground(Color.white);
				Field_18.setBackground(Color.white);
				Field_19.setBackground(Color.white);
				Field_20.setBackground(Color.white);
				Field_21.setBackground(Color.white);
				Field_22.setBackground(Color.white);
				Field_23.setBackground(Color.white);
				Field_24.setBackground(Color.white);
				Field_25.setBackground(Color.white);
				Field_26.setBackground(Color.white);
				Field_27.setBackground(Color.white);
				Field_28.setBackground(Color.white);
				Field_29.setBackground(Color.white);
			}
		});
		btnKombinacija.setFont(new Font("Arial", Font.BOLD, 12));
		btnKombinacija.setBackground(new Color(143, 188, 143));
		btnKombinacija.setBounds(419, 22, 142, 23);
		
		panel_6.add(btnKombinacija);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(218, 165, 32));
		tabbedPane.addTab("Nagrada", null, panel_3, null);
		panel_3.setLayout(null);
		textArea.setBackground(new Color(102, 255, 255));
		textArea.setFont(new Font("Miriam Fixed", Font.BOLD, 14));
		
		
		textArea.setBounds(10, 11, 556, 186);
		panel_3.add(textArea);
		btnOdgovori.setForeground(new Color(0, 0, 0));
		btnOdgovori.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					
				if(rdbtnKanada.isSelected())					
					brB=brB+500;
					brTac=brTac+1;
				if(rdbtnRusija.isSelected())					
					brB=brB-100;
				brNet++;
				if(rdbtnAustralija.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnKina.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnRusija_1.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnSad.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnAmazon.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnMisisipi.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnJangcekjang.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnKursumlija.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnLeskovac.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnProkuplje.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnSubotica.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnSombor.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnVrsac.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnNewRadioButton_1.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnNewRadioButton.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnNewRadioButton_2.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnTenis.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnPlivanje.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnFudbal.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnOldTrafford.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnWhiteHartLane.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnCampNou.isSelected())
					brB=brB-100;
				brNet++;
				if(radioButton_2.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(radioButton.isSelected())
					brB=brB-100;
				brNet++;
				if(radioButton_1.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnDelPotro.isSelected())
					brB=brB+500;
				brTac=brTac+1;
				if(rdbtnDjokovic.isSelected())
					brB=brB-100;
				brNet++;
				if(rdbtnFederer.isSelected())
					brB=brB-100;
				brNet++;
				textArea.setText(Double.toString(brB));
				textArea.setText(null);				
				
				for(Takmicar c:ls)	
				{
				textArea.setText("\n"+c.ime+" "+c.prezime+ " ima "+c.god+" godina "+"i ovojio je: " +brB+ " dinara\n "
						+ "broj tacnih odgovora:"+brTac+"\n"+"broj netacnih odgovora:"+brNet);	
				}
			}
		});
		ButtonGroup grupa65= new ButtonGroup();
		grupa65.add(Button_1);
		grupa65.add(Button_2);
		grupa65.add(Button_3);
		grupa65.add(Button_4);
		grupa65.add(Button_5);
		grupa65.add(Button_6);
		grupa65.add(Button_7);
		grupa65.add(Button_8);
		grupa65.add(Button_9);
		grupa65.add(Button_10);
		grupa65.add(Button_11);
		grupa65.add(Button_12);
		grupa65.add(Button_13);
		grupa65.add(Button_14);
		grupa65.add(Button_15);
		grupa65.add(Button_16);
		grupa65.add(Button_17);
		grupa65.add(Button_18);
		grupa65.add(Button_19);
		grupa65.add(Button_20);
		grupa65.add(Button_21);
		grupa65.add(Button_22);
		grupa65.add(Button_23);
		grupa65.add(Button_24);
		
		btnOdgovori.setBackground(new Color(32, 178, 170));
		btnOdgovori.setFont(new Font("Arial", Font.BOLD, 13));
		btnOdgovori.setBounds(209, 223, 145, 23);
		
		panel_3.add(btnOdgovori);
	}
}