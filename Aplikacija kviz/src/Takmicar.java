
public class Takmicar {
public String ime,prezime;
public int god;

public Takmicar(String ime, String prezime, int god) {
	super();
	this.ime = ime;
	this.prezime = prezime;
	this.god = god;
}

@Override
public String toString() {
	return ("Takmicar " + ime + " " + prezime + " rodjen " + god );
}

}
